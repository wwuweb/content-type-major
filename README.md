# Major
This feature contains the Major content type.

## Requirements
* [Alumni Content Type](https://bitbucket.org/wwuweb/content-type-alumni)

## Installation Instructions
### 1) Install the Alumni Content Type
Follow the instructions on the Alumni Content Type readme

### 2) Download this module
Clone this repository into your `site/web/modules/contrib/` folder

### 3) Install Other Modules
The Major content type is dependent on a number of modules that are included in the install profile, but are not necessarily enabled by default. To find out if any of these modules still need to be installed, in your browser go to *site/admin/modules* and find the **Major** module under the **Other** heading. Click the text to expand, and install any of the indicated modules listed by *Requires*

  >If you run into errors while trying to enable these modules, try installing them one at a time rather than all at once.

### 4) Enable this module
After the required modules are installed, select this module from the *site/admin/modules* screen and click the Install button.
